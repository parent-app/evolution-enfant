package com.openclassrooms.parentapp.croissanceenfant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParentAppCroissanceEnfantApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParentAppCroissanceEnfantApplication.class, args);
	}

}

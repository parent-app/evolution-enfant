package com.openclassrooms.parentapp.croissanceenfant.controller;

import com.openclassrooms.parentapp.croissanceenfant.dto.AbstractEntityDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.FoodDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.FoodSuggestionDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetFoodDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.FoodSearch;
import com.openclassrooms.parentapp.croissanceenfant.service.DiversificationPeriodService;
import com.openclassrooms.parentapp.croissanceenfant.service.FoodService;
import com.openclassrooms.parentapp.croissanceenfant.service.FoodTypeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(DietaryDiversificationController.ROOT_RESOURCE)
@Tag(name = "dietaryDiversifications", description = "Handler for dietary diversification requests")
@CrossOrigin(origins = "*")
public class DietaryDiversificationController {

    public static final String ROOT_RESOURCE = "/api/dietary-diversifications";
    public static final String PERIOD_RESOURCE = "/periods";
    public static final String TYPE_RESOURCE = "/types";
    public static final String CHILD_RESOURCE = "/children";
    private final FoodService foodService;
    private final FoodTypeService foodTypeService;
    private final DiversificationPeriodService diversificationPeriodService;

    @GetMapping
    @Operation(summary = "Get all foods corresponding to the type and/or period requested")
    public List<FoodDto> getFoods(@Parameter(name = "period", description = "id of the requested period") @RequestParam(required = false) Long period,
                                  @Parameter(name = "type", description = "id of the requested type") @RequestParam(required = false) Long type) {
        return foodService.findAll(new FoodSearch(type, period));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get food by id")
    public FoodDto getFood(@Parameter(name = "id", description = "id of the requested food") @PathVariable Long id) {
        return foodService.findById(id);
    }

    @GetMapping("/suggest")
    @Operation(summary = "Suggest food by name")
    public List<FoodSuggestionDto> suggestFood(@Parameter(name = "name", description = "name on which we want to search for food") @RequestParam String name) {
        return foodService.suggestFood(name);
    }

    @GetMapping(DietaryDiversificationController.PERIOD_RESOURCE)
    @Operation(summary = "Get all periods")
    public List<AbstractEntityDto> getPeriods() {
        return diversificationPeriodService.findAll();
    }

    @GetMapping(DietaryDiversificationController.TYPE_RESOURCE)
    @Operation(summary = "Get all types")
    public List<AbstractEntityDto> getTypes() {
        return foodTypeService.findAll();
    }

    @GetMapping(DietaryDiversificationController.CHILD_RESOURCE)
    @Operation(summary = "Get child food")
    public List<WidgetFoodDto> getChildFood(@Parameter(name = "birthDate", description = "Child birthdate")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate birthDate) {
        return foodService.findChildFood(birthDate);
    }

}

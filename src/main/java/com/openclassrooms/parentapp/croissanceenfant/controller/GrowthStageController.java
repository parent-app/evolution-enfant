package com.openclassrooms.parentapp.croissanceenfant.controller;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStageDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStagePeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStageTypeDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetGrowthStageDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStageSearch;
import com.openclassrooms.parentapp.croissanceenfant.service.GrowthStagePeriodService;
import com.openclassrooms.parentapp.croissanceenfant.service.GrowthStageService;
import com.openclassrooms.parentapp.croissanceenfant.service.GrowthStageTypeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(GrowthStageController.ROOT_RESOURCE)
@Tag(name = "growthstages", description = "Handler for growthStage requests")
@CrossOrigin(origins = "*")
public class GrowthStageController {

    public static final String ROOT_RESOURCE = "/api/growth-stages";
    public static final String PERIOD_RESOURCE = "/periods";
    public static final String TYPE_RESOURCE = "/types";
    public static final String CHILD_RESOURCE = "/children";
    private final GrowthStageService growthStageService;
    private final GrowthStagePeriodService growthStagePeriodService;
    private final GrowthStageTypeService growthStageTypeService;

    @GetMapping
    @Operation(summary = "Get all growth stages. Can filter request by type or period")
    public List<GrowthStageDto> getGrowthStages(@Parameter(name = "periods", description = "list of period ids to filter the request") @RequestParam(required = false) List<Long> periods,
                                                @Parameter(name = "types", description = "list of type ids to filter the request") @RequestParam(required = false) List<Long> types) {
        GrowthStageSearch search = new GrowthStageSearch(types, periods);
        return growthStageService.findAll(search);
    }

    @GetMapping(GrowthStageController.PERIOD_RESOURCE)
    @Operation(summary = "Get all growth stage periods")
    public List<GrowthStagePeriodDto> getGrowthStagePeriods() {
        return growthStagePeriodService.findAll();
    }

    @GetMapping(GrowthStageController.TYPE_RESOURCE)
    @Operation(summary = "Get all growth stage types")
    public List<GrowthStageTypeDto> getGrowthStageTypes() {
        return growthStageTypeService.findAll();
    }

    @GetMapping(GrowthStageController.CHILD_RESOURCE)
    @Operation(summary = "Get child growth stage period")
    public List<WidgetGrowthStageDto> getChildGrowthStage(@Parameter(name = "birthDate", description = "Child birthdate")
                                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate birthDate) {
        return growthStageService.findChildGrowthStage(birthDate);
    }

}

package com.openclassrooms.parentapp.croissanceenfant.controller;

import com.openclassrooms.parentapp.croissanceenfant.dto.LightVaccineDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.PeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.VaccineDto;
import com.openclassrooms.parentapp.croissanceenfant.service.PeriodService;
import com.openclassrooms.parentapp.croissanceenfant.service.VaccineService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(VaccineController.ROOT_RESOURCE)
@Tag(name = "vaccines", description = "Handler for vaccines request")
@CrossOrigin(origins = "*")
public class VaccineController {

    public static final String ROOT_RESOURCE = "/api/vaccines";
    public static final String PERIOD_RESOURCE = "/periods";
    public static final String CHILD_RESOURCE = "/children";
    private final VaccineService vaccineService;
    private final PeriodService periodService;

    @GetMapping
    @Operation(summary = "Get all vaccines. Can filter request by period")
    public List<VaccineDto> getVaccines(@Parameter(name = "periods", description = "id of the period to filter the request") @RequestParam(required = false) List<Long> periods) {
        return vaccineService.getAllVaccine(periods);
    }

    @GetMapping(CHILD_RESOURCE)
    @Operation(summary = "Get child next vaccines")
    public List<LightVaccineDto> getChildNextVaccines(@Parameter(name = "birthDate", description = "Child birthdate")
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate birthDate) {
        return vaccineService.getChildNextVaccines(birthDate);
    }

    @GetMapping(PERIOD_RESOURCE)
    @Operation(summary = "Get all periods")
    public List<PeriodDto> getPeriods() {
        return periodService.getAllPeriods();
    }
}

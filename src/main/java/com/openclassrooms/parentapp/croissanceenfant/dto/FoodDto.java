package com.openclassrooms.parentapp.croissanceenfant.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FoodDto {

    private Long id;
    private String name;
    private AbstractEntityDto type;
    private String comment;
    private AbstractEntityDto period;
}

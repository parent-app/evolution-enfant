package com.openclassrooms.parentapp.croissanceenfant.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GrowthStageDto {

    private Long id;
    private GrowthStagePeriodDto period;
    private GrowthStageTypeDto type;
    private String description;
}

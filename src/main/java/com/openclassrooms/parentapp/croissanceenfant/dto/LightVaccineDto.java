package com.openclassrooms.parentapp.croissanceenfant.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LightVaccineDto {

    private Long id;
    private String name;
    private List<VaccinePeriodDto> vaccinePeriods;
}

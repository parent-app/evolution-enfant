package com.openclassrooms.parentapp.croissanceenfant.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VaccineDto {

    private Long id;
    private String name;
    private String description;
    private boolean mandatory;
    private List<VaccinePeriodDto> vaccinePeriods;
}

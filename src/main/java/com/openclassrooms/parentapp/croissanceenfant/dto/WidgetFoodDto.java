package com.openclassrooms.parentapp.croissanceenfant.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WidgetFoodDto {

    private String name;
    private String type;
    private String period;
}

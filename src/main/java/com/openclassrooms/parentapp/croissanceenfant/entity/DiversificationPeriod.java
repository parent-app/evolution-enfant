package com.openclassrooms.parentapp.croissanceenfant.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name = "t_diversification_period", schema = "dietary_diversification")
public class DiversificationPeriod extends AbstractEntity {

    @Column(name = "min_month_value")
    private Short minMonthValue;

    @Column(name = "max_month_value")
    private Short maxMonthValue;
}

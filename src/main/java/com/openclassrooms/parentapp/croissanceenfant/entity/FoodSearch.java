package com.openclassrooms.parentapp.croissanceenfant.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FoodSearch {

    private Long type;
    private Long period;
}

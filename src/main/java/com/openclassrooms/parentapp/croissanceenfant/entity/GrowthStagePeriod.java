package com.openclassrooms.parentapp.croissanceenfant.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name = "t_growth_stage_period", schema = "child_growth")
public class GrowthStagePeriod extends AbstractEntity {

    @Column(name = "min_month_value")
    private Short minMonthValue;

    @Column(name = "max_month_value")
    private Short maxMonthValue;
}

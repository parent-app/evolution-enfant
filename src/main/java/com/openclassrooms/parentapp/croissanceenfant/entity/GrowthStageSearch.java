package com.openclassrooms.parentapp.croissanceenfant.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrowthStageSearch {

    private List<Long> typeId;
    private List<Long> periodId;
}

package com.openclassrooms.parentapp.croissanceenfant.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name = "t_vaccination_period", schema = "child_vaccine")
public class VaccinationPeriod extends AbstractEntity {

    @OneToMany(mappedBy = "period")
    private List<VaccinePeriod> vaccinesPeriods;

    @Column(name = "month_value")
    private Short monthValue;
}

package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.AbstractEntityDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.AbstractEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AbstractEntityMapper {

    List<AbstractEntityDto> toAbstractEntityDtoList(Iterable<? extends AbstractEntity> entity);
}

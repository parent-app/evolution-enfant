package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.FoodDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.FoodSuggestionDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetFoodDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.Food;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = AbstractEntityMapper.class)
public interface FoodMapper {

    FoodDto toFoodDto(Food food);
    List<FoodSuggestionDto> toFoodSuggestionDto(List<Food> food);
    List<FoodDto> toFoodDtoList(Iterable<Food> foodList);


    @Mapping(target = "type", source = "type.label")
    @Mapping(target = "period", source = "period.label")
    WidgetFoodDto toWidgetFoodDto(Food food);

    @Mapping(target = "type", source = "type.label")
    @Mapping(target = "period", source = "period.label")
    List<WidgetFoodDto> toWidgetFoodDtoList(List<Food> foodList);
}

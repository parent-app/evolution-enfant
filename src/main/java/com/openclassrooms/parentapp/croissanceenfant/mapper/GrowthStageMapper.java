package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStageDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetGrowthStageDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {GrowthStagePeriodMapper.class, GrowthStageTypeMapper.class})
public interface GrowthStageMapper {

    GrowthStageDto toGrowthStageDto(GrowthStage growthStage);
    List<GrowthStageDto> toGrowthStageDtoList(Iterable<GrowthStage> growthStages);

    @Mapping(target = "period", source = "period.label")
    @Mapping(target = "type", source = "type.label")
    WidgetGrowthStageDto toWidgetGrowthStageDto(GrowthStage growthStage);

    @Mapping(target = "period", source = "period.label")
    @Mapping(target = "type", source = "type.label")
    List<WidgetGrowthStageDto> toWidgetGrowthStageDtoList(List<GrowthStage> growthStages);
}

package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStagePeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStagePeriod;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GrowthStagePeriodMapper {

    GrowthStagePeriodDto toGrowthStagePeriodDto(GrowthStagePeriod growthStagePeriod);
    List<GrowthStagePeriodDto> toGrowthStagePeriodDtoList(Iterable<GrowthStagePeriod> growthStagePeriods);
}

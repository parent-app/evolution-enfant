package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStageTypeDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStageType;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GrowthStageTypeMapper {

    GrowthStageTypeDto toGrowthStageTypeDto(GrowthStageType growthStageType);
    List<GrowthStageTypeDto> toGrowthStageTypeListDto(Iterable<GrowthStageType> growthStageTypes);
}

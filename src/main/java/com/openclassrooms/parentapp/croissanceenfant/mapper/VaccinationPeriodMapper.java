package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.PeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.VaccinationPeriod;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface VaccinationPeriodMapper {

    List<PeriodDto> toPeriodDtoList(Iterable<VaccinationPeriod> periods);
}

package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.VaccineDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.Vaccine;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = VaccinePeriodMapper.class)
public interface VaccineMapper {

    List<VaccineDto> toVaccineDtoList(Iterable<Vaccine> vaccines);
}

package com.openclassrooms.parentapp.croissanceenfant.mapper;

import com.openclassrooms.parentapp.croissanceenfant.dto.VaccinePeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.VaccinePeriod;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = VaccinationPeriodMapper.class)
public interface VaccinePeriodMapper {

    List<VaccinePeriodDto> toVaccinePeriodDtoList(Iterable<VaccinePeriod> vaccinePeriods);
}

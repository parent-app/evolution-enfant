package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.DiversificationPeriod;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiversificationPeriodRepository extends CrudRepository<DiversificationPeriod, Long> {
    @Query("SELECT dp.id FROM DiversificationPeriod  dp WHERE dp.minMonthValue <= :monthValue AND dp.maxMonthValue > :monthValue")
    List<Long> findChildPeriods(@Param("monthValue") short monthValue);
}

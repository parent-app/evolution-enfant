package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.Food;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends CrudRepository<Food, Long>, QuerydslPredicateExecutor<Food> {

    List<Food> findByNameContainingIgnoreCase(String name);
    List<Food> findByPeriodIdIn(List<Long> ids);
}

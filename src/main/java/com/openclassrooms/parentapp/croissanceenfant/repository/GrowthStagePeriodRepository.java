package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStagePeriod;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GrowthStagePeriodRepository extends CrudRepository<GrowthStagePeriod, Long> {

    @Query("SELECT gsp.id FROM GrowthStagePeriod  gsp WHERE gsp.minMonthValue <= :monthValue AND gsp.maxMonthValue > :monthValue")
    Long findChildGrowthStagePeriod(@Param("monthValue") short monthValue);
}

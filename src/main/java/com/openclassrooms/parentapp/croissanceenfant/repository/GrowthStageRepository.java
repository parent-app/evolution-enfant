package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStage;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrowthStageRepository extends CrudRepository<GrowthStage, Long>, QuerydslPredicateExecutor<GrowthStage> {

    List<GrowthStage> findByPeriodId(Long periodId);
}

package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStageType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrowthStageTypeRepository extends CrudRepository<GrowthStageType, Long> {
}

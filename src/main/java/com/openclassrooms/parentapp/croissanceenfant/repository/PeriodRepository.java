package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.VaccinationPeriod;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodRepository extends CrudRepository<VaccinationPeriod, Long> {

    VaccinationPeriod findFirstByMonthValueGreaterThanEqual(short monthValue);
}

package com.openclassrooms.parentapp.croissanceenfant.repository;

import com.openclassrooms.parentapp.croissanceenfant.entity.Vaccine;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VaccineRespository extends CrudRepository<Vaccine, Long>{

    List<Vaccine> findDistinctByVaccinePeriodsPeriodIdIn(List<Long> periodIds);
}

package com.openclassrooms.parentapp.croissanceenfant.repository.predicate;

import com.openclassrooms.parentapp.croissanceenfant.entity.FoodSearch;
import com.openclassrooms.parentapp.croissanceenfant.entity.QFood;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.Predicate;

public class FoodPredicateBuilder {

    public static final QFood food = QFood.food;

    private FoodPredicateBuilder() {
        throw new IllegalStateException("Utility class");
    }

    public static Predicate buildSearch(FoodSearch search) {
        return new BooleanBuilder()
                .and(isType(search.getType()))
                .and(isPeriod(search.getPeriod()));
    }

    private static BooleanExpression isType(Long type) {
        return type != null ? food.type.id.eq(type) : null;
    }

    private static BooleanExpression isPeriod(Long period) {
        return period != null ? food.period.id.eq(period) : null;
    }
}

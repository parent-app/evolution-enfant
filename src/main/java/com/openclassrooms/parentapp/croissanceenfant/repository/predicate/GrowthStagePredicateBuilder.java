package com.openclassrooms.parentapp.croissanceenfant.repository.predicate;

import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStageSearch;
import com.openclassrooms.parentapp.croissanceenfant.entity.QGrowthStage;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.List;

public class GrowthStagePredicateBuilder {

    public static final QGrowthStage growthStage = QGrowthStage.growthStage;

    private GrowthStagePredicateBuilder() {
        throw new IllegalStateException("Utility class");
    }

    public static Predicate buildSearch(GrowthStageSearch search) {
        return new BooleanBuilder()
                .and(isPeriod(search.getPeriodId()))
                .and(isType(search.getTypeId()));
    }

    private static BooleanExpression isPeriod(List<Long> periods) {
        return periods != null ? growthStage.period.id.in(periods) : null;
    }

    private static BooleanExpression isType(List<Long> types) {
        return types != null ? growthStage.type.id.in(types) : null;
    }
}

package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.AbstractEntityDto;
import com.openclassrooms.parentapp.croissanceenfant.mapper.AbstractEntityMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.DiversificationPeriodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DiversificationPeriodService {

    private final DiversificationPeriodRepository diversificationPeriodRepository;
    private final AbstractEntityMapper abstractEntityMapper;

    public List<AbstractEntityDto> findAll() {
        return abstractEntityMapper.toAbstractEntityDtoList(diversificationPeriodRepository.findAll());
    }
}

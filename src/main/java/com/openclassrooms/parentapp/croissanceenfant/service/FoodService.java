package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.FoodDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.FoodSuggestionDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetFoodDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.FoodSearch;
import com.openclassrooms.parentapp.croissanceenfant.exception.AppEntityNotFoundException;
import com.openclassrooms.parentapp.croissanceenfant.mapper.FoodMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.DiversificationPeriodRepository;
import com.openclassrooms.parentapp.croissanceenfant.repository.FoodRepository;
import com.openclassrooms.parentapp.croissanceenfant.repository.predicate.FoodPredicateBuilder;
import com.openclassrooms.parentapp.croissanceenfant.utils.ChildUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodService {

    private final FoodRepository foodRepository;
    private final FoodMapper foodMapper;
    private final DiversificationPeriodRepository diversificationPeriodRepository;

    public List<FoodDto> findAll(FoodSearch foodSearch) {
        return foodMapper.toFoodDtoList(foodRepository.findAll(FoodPredicateBuilder.buildSearch(foodSearch)));
    }

    public FoodDto findById(Long id) {
        return foodMapper.toFoodDto(foodRepository.findById(id).orElseThrow(AppEntityNotFoundException::new));
    }

    public List<FoodSuggestionDto> suggestFood(String name) {
        return foodMapper.toFoodSuggestionDto(foodRepository.findByNameContainingIgnoreCase(name));
    }

    public List<WidgetFoodDto> findChildFood(LocalDate birthDate) {
        long childMonth = ChildUtils.getChildAge(birthDate);

        List<Long> diversificationPeriods = diversificationPeriodRepository.findChildPeriods((short) childMonth);
        return foodMapper.toWidgetFoodDtoList(foodRepository.findByPeriodIdIn(diversificationPeriods));
    }
}

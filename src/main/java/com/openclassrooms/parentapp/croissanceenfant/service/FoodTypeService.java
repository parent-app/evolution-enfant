package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.AbstractEntityDto;
import com.openclassrooms.parentapp.croissanceenfant.mapper.AbstractEntityMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.FoodTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodTypeService {

    private final FoodTypeRepository foodTypeRepository;
    private final AbstractEntityMapper abstractEntityMapper;

    public List<AbstractEntityDto> findAll() {
        return abstractEntityMapper.toAbstractEntityDtoList(foodTypeRepository.findAll());
    }
}

package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStagePeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.mapper.GrowthStagePeriodMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.GrowthStagePeriodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GrowthStagePeriodService {

    private final GrowthStagePeriodRepository growthStagePeriodRepository;
    private final GrowthStagePeriodMapper growthStagePeriodMapper;

    public List<GrowthStagePeriodDto> findAll() {
        return growthStagePeriodMapper.toGrowthStagePeriodDtoList(growthStagePeriodRepository.findAll());
    }
}

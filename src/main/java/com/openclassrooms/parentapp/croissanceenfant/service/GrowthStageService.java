package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStageDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetGrowthStageDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.GrowthStageSearch;
import com.openclassrooms.parentapp.croissanceenfant.mapper.GrowthStageMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.GrowthStagePeriodRepository;
import com.openclassrooms.parentapp.croissanceenfant.repository.GrowthStageRepository;
import com.openclassrooms.parentapp.croissanceenfant.repository.predicate.GrowthStagePredicateBuilder;
import com.openclassrooms.parentapp.croissanceenfant.utils.ChildUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GrowthStageService {

    private final GrowthStageRepository growthStageRepository;
    private final GrowthStageMapper growthStageMapper;
    private final GrowthStagePeriodRepository growthStagePeriodRepository;

    public List<GrowthStageDto> findAll(GrowthStageSearch search) {
        return growthStageMapper.toGrowthStageDtoList(growthStageRepository.findAll(GrowthStagePredicateBuilder.buildSearch(search)));
    }

    public List<WidgetGrowthStageDto> findChildGrowthStage(LocalDate birthDate) {
        long childMonth = ChildUtils.getChildAge(birthDate);

        return growthStageMapper.toWidgetGrowthStageDtoList(
                growthStageRepository.findByPeriodId(
                        growthStagePeriodRepository.findChildGrowthStagePeriod((short) childMonth))
        );
    }
}

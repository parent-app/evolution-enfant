package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.GrowthStageTypeDto;
import com.openclassrooms.parentapp.croissanceenfant.mapper.GrowthStageTypeMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.GrowthStageTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GrowthStageTypeService {

    private final GrowthStageTypeRepository growthStageTypeRepository;
    private final GrowthStageTypeMapper growthStageTypeMapper;

    public List<GrowthStageTypeDto> findAll() {
        return growthStageTypeMapper.toGrowthStageTypeListDto(growthStageTypeRepository.findAll());
    }
}

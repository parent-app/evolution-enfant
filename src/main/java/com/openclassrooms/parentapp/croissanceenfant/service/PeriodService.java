package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.PeriodDto;
import com.openclassrooms.parentapp.croissanceenfant.mapper.VaccinationPeriodMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.PeriodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PeriodService {

    private final VaccinationPeriodMapper vaccinationPeriodMapper;
    private final PeriodRepository periodRepository;

    public List<PeriodDto> getAllPeriods() {
        return this.vaccinationPeriodMapper.toPeriodDtoList(this.periodRepository.findAll());
    }
}

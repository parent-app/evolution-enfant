package com.openclassrooms.parentapp.croissanceenfant.service;

import com.openclassrooms.parentapp.croissanceenfant.dto.LightVaccineDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.VaccineDto;
import com.openclassrooms.parentapp.croissanceenfant.entity.Vaccine;
import com.openclassrooms.parentapp.croissanceenfant.mapper.VaccineMapper;
import com.openclassrooms.parentapp.croissanceenfant.mapper.VaccinePeriodMapper;
import com.openclassrooms.parentapp.croissanceenfant.repository.PeriodRepository;
import com.openclassrooms.parentapp.croissanceenfant.repository.VaccineRespository;
import com.openclassrooms.parentapp.croissanceenfant.utils.ChildUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VaccineService {

    private final VaccineMapper vaccineMapper;
    private final VaccineRespository vaccineRespository;
    private final PeriodRepository periodRepository;
    private final VaccinePeriodMapper vaccinePeriodMapper;

    public List<VaccineDto> getAllVaccine(List<Long> periodsId) {
        if (periodsId != null && !periodsId.isEmpty()) {
            return this.vaccineMapper.toVaccineDtoList(this.vaccineRespository.findDistinctByVaccinePeriodsPeriodIdIn(periodsId));
        }
        return vaccineMapper.toVaccineDtoList(this.vaccineRespository.findAll());
    }

    public List<LightVaccineDto> getChildNextVaccines(LocalDate birthDate) {
        long childMonth = ChildUtils.getChildAge(birthDate);

        var vaccinationPeriod = periodRepository.findFirstByMonthValueGreaterThanEqual((short) childMonth);

        var vaccinationPeriodsId = List.of(vaccinationPeriod.getId(), vaccinationPeriod.getId() + 1);

        var vaccines = vaccineRespository.findDistinctByVaccinePeriodsPeriodIdIn(vaccinationPeriodsId);

        return buildLightVaccineDto(vaccines, vaccinationPeriodsId);
    }

    private List<LightVaccineDto> buildLightVaccineDto(List<Vaccine> vaccines, List<Long> vaccinationPeriodsId) {
        List<LightVaccineDto> result = new ArrayList<>();
        vaccines.forEach(vaccine ->
            result.add(LightVaccineDto.builder()
                    .id(vaccine.getId())
                    .name(vaccine.getName())
                    .vaccinePeriods(vaccinePeriodMapper.toVaccinePeriodDtoList(vaccine.getVaccinePeriods().stream().filter(vaccinePeriod -> vaccinationPeriodsId.contains(vaccinePeriod.getPeriod().getId())).collect(Collectors.toList())))
                    .build())
        );
        return result;
    }
}

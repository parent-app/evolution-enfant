package com.openclassrooms.parentapp.croissanceenfant.utils;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public final class ChildUtils {

    private ChildUtils() {}

    public static long getChildAge(LocalDate birthDate) {
        return ChronoUnit.MONTHS.between(
                birthDate.withDayOfMonth(1),
                LocalDate.now().withDayOfMonth(1));
    }
}

CREATE SCHEMA IF NOT EXISTS child_growth;

CREATE TABLE child_growth.t_growth_stage_period
(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR(50) NOT NULL
);

CREATE TABLE child_growth.t_growth_stage_type
(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR(50) NOT NULL
);


CREATE TABLE child_growth.t_growth_stage
(
    id BIGSERIAL PRIMARY KEY,
    period_id BIGSERIAL NOT NULL,
    type_id BIGSERIAL NOT NULL,
    description TEXT NOT NULL,
    CONSTRAINT FK_GROWTHSTAGE_PERIOD FOREIGN KEY (period_id) REFERENCES t_growth_stage_period(id),
    CONSTRAINT FK_GROWTHSTAGE_TYPE FOREIGN KEY (type_id) REFERENCES t_growth_stage_type(id)
);

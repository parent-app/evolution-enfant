INSERT INTO t_growth_stage_period (label) VALUES
('0 à 3 mois'),
('3 à 6 mois'),
('6 à 9 mois'),
('9 mois à 1 an'),
('12 à 18 mois'),
('18 à 24 mois'),
('24 mois à 3 ans'),
('3 à 6 ans');

INSERT INTO t_growth_stage_type (label) VALUES
('Développement psychomoteur'),
('Développement sensoriel'),
('Développement du langage'),
('Développement affectif et social'),
('Jeux/jouets'),
('Activité et rôle de l''adulte');

INSERT INTO t_growth_stage(period_id, type_id, description)
VALUES
(
    1,
    1,
    'Vers 3 mois, le nourrisson commence à tenir sa tête droite. Préhension involontaire au contact d''objets placés dans sa main'
),
(
    1,
    2,
    'Vue : Sensible au contraste, reconnait le visage de sa mère, champs visuel limité à 30cm. A deux mois, il suit des yeux les objets déplacés sur 180°. Audition : sensible aux sons et tons aigus. Reconnait la voix de ses parents. Odorat : réagit aux odeurs de lait et de la peau maternelle. Gout : continue à se développer. Toucher : a besoin de toucher, d''être câliné'
),
(
    1,
    4,
    'A besoin que l''adulte réponde rapidement et de façon adaptée à ses demandes pour se sentir en sécurité. Les périodes d''éveil (encore réduites) vont lui permettre d''entrer en contact avec autrui. Il est dans une relation symbiotique avec sa mère, puis s''installe la dynamique de la triade père/mère/enfant. Il s''exprime par des gazouillis, pleurs, détente et les sourires en réponse à la satisfaction de ses besoins. Le sourire-réponse apparait vers 2 mois.'
),
(
    1,
    5,
    'Jouets sonores : hochets, grelos, culbutos. Peluches et jouets en tissus. Tapis d''éveil. Mobiles, bouliers aux couleurs contrastées. Musiques douces, comptines, chansons.'
),
(
    1,
    6,
    'Faire découvrir les odeurs. Bercements et câlins pour se sentir en sécurité. Il est sensible aux changements de position et au portage dans les bras. Massage de détente du corps si le nourrisson est réceptif. Besoin de communiquer par le langage et le regard. Promenades participent au bien être du bébé.'
),
(
    2,
    1,
    '4 mois : il est capable de tenir sa tête dans l''alignement de son corps et il commence à se déplacer dans sont lit par rotation. 5 mois : il peut soulever le buste en prenant appui sur les avant-bras et peut se retourner sur le côté. Pédale avec les jambes. Peut saisir un objet avec 4 doigts. 6 mois : commence à se tenir assis avec appui et se retourner seul du ventre sur le dos. Aime jouer avec ses pieds et les porter à la bouche. Il peut tenir des petits cubes dans ses mains.'
),
(
    2,
    2,
    'A partir de 3mois, le bébé voit au delà de 2.50 mètres. Il voit précisément les visages, les objets et distingue les couleurs. Il découvre un peut plus son environnement grâce à ses 5 sens qui s''affinent. Besoin de tout porter à la bouche'
),
(
    2,
    4,
    'Il découvre le plaisir de l''échange par les vocalises, les sourires-réponses, les mimiques, puis le sourire sélectif. Il rit aux éclats. Il prend conscience des sons qu''il émet et les répète. Miroir : Le bébé s''intéresse surtout à l''image de la personne qui le porte. Il différencie sa mère de son père, une personne familière d''un inconnu. Il cherche activement à obtenir la proximité de la personne qui s''occupe de lui.'
),
(
    2,
    5,
    'Tapis de jeux et d''éveil, tissus différents, cubes en mousse, hochets musicaux. Portique par courte période. Musiques douces, comptines, mobile au dessus du lit.'
),
(
    2,
    6,
    'Proposer différentes situations corporelles : à plat dos, sur le côté, le ventre. Massage de détente : permet la prise de conscience corporelle. Promenade en poussette demi-assise : découverte du monde extérieur. Adapter le jeu de l''enfant à son âge et sa réceptivité. Il peut jouer seul. Renouveler les jeux afin de ne pas le lasser. Respecter son rythme. Etre réceptif aux sourires, regards, vocalises, les encourager et y répondre'
),
(
    3,
    1,
    '7 mois : peut rester assis sans soutien. Acquiert la préhension en pince. Fait passer le jouet d''une main à l''autre. Sait tendre les bras. Dit plusieurs syllabes. 8 mois : à plat dos, il peut se soulever jusqu''à la position assise. Peut rester assis sans soutien et peut rouler sur lui même. Commence à ramper. Inquiet face à des visages étrangers. Applaudit, fait les marionnettes, dit au revoir. Il comprend le sens du non. 9 mois : se déplace à 4 pattes, se met debout en s''accrochant aux meubles. Peut saisir les objets entre le pouce et l''index. Comprend le sens d''ordres simples. Associe différents types de syllabes.'
),
(
    3,
    2,
    'A partir du 7ème mois, sa vision et son audition s''affinent. Le gout se développe, il peut refuser de manger.'
),
(
    3,
    4,
    'Le bébé a peur des inconnus. Il peut mal réagir quand il est séparé de ses parents. Le bébé reconnait que sa mère est différente de lui et des autres personnes. Cela va le conduire petit à petit vers l''autonomie. Certains enfants vont trouver du réconfort dans le contact d''un objet privilégie comme le doudou par exemple (objet transitionnel). Cet objet va lui permettre se supporter l''absence de sa mère et de faire le lien entre la maison et l''extérieur.'
),
(
    3,
    5,
    'Tableaux d''éveil, téléphone musical, poupées en tissus, cuves, boîtes gigognes, petit ballon. Jouets en différentes matières. Formes à empiler, encastrer, emboîter. Jeu de cache-cache (qui permet à l''enfant de dépasser l''angoisse de l''abandon). Il jette ses jouets pour que l''adulte les lui restitue.'
),
(
    3,
    6,
    'Accompagner le bébé dans le jeu, le rassurer, le stimuler, le sécuriser. Lire des petites histoires, chanter, mimer des comptines. Ecouter de la musique douce. Accompagner les gestes de la vie quotidienne par la parole.'
),

(
    4,
    1,
    'Les progrès psychomoteurs sont importants pendant cette période. Le bébé prend conscience de la partie inférieure de son corps. Il tient assis avec stabilité. Se déplace à 4 pattes ou sur les fesses. Se met debout avec appui, puis sans appui. Commence à marcher avec soutien ou tenu par la main. Attrape des objets avec plus de précision. Il demande à tenir sa cuillère et à boire tout seul au verre.'
),
(
    4,
    3,
    'Le bébé s''adresse à ses parents en disant "papa" et "maman". Il associe le geste à la parole. Il répète de plus en plus de syllabes et dit quelques mots.'
),
(
    4,
    4,
    'Le bébé montre du doigt ce qui l''intéresse. Il comprend un interdit, les expressions du visage de l''adulte, le sens de quelques mots ainsi que des ordres simples. Il est capable de retrouver un objet caché. Il aime faire des mimiques, du charme. Il essaie d''imiter ce qu''il observe. Ses actions sont intentionnelles. Il réfléchit et reproduit ce qu''il a déjà expérimenté. Le bébé a besoin du soutient de ses parents pour gérer ses émotions.'
),
(
    4,
    5,
    'C''est l''âge de l''exploration, il examine tout inlassablement car tout l''amuse et l''intéresse. Il aime les mêmes jouets qu''avant mais diversifie leur utilisation. Le moindre objet se transforme en jouet. Il apprécie particulièrement les jouets musicaux. Il aime jouer à trainer, pousser, les jeux d''eau. Il s''approprie le livre, tourne les pages, le mord, etc. Il adore sortir se promener, découvrir le monde extérieur.'
),
(
    4,
    6,
    'Encourager et accompagner le bébé dans ses progrès vers l''autonomie sans pour autant le sur-stimuler ni le freiner. Poser au bébé des limites nécessaires face aux dangers liés à l''exploration de l''environnement. Se concentrer sur ses progrès, son bien-être général, son plaisir à découvrir et à évoluer plutôt que sur ses difficultés.'
),
(
     5,
     1,
    'Il marche tenu d''une main puis seul. Il monte les marches avec aide, s''accroupi en jouant, pointe l''index vers les objets. Il commence à imiter les gestes de l''adulte. Sa préhension est affinée. Il sait manger et boire seul (14-15 mois). Il tourne les pages d''un livre. Il explore les trous, découvre le contenant et le contenu. Il découvre les crayons et la joie de leur utilisation. Il aime jeter, pousser, renvoyer, faire une tour de plusieurs cubes, taper dans un ballon sans tomber, jeter une balle. Il montre les parties de son visage et de son corps à la demande.'
),
(
    5,
    3,
    'Il dit quelques mots à 12 mois. A 18 mois, il associe deux mots et peut nommer des images connues.'
),
(
    5,
    4,
    'Il aime les jeux d''échange avec l''adulte. Il reconnait les personnes de son entourage et est méfiant vis-à-vis des personnes inconnues. Il participe à son déshabillage. Il commence à s''intéresser aux autres enfants : ils jouent les uns à côté des autres et peuvent convoiter le même jouet. L''enfant entre dans l''opposition à l''adulte afin de montrer qu''il est différent et unique (étape nécessaire pour qu''il puisse affirmer sa personnalité).'
),
(
    5,
    5,
    'Ballon. Jouets à tirer, à pousser, à bousculer. Jeux à encastrer et assembler. Porteurs. Catalogues à déchirer, tubes, boîtes, cartons. Jeux de sable et d''eau, pâte à sel et à modeler. Dînette. Marionnettes. Toboggans. Tricycles. Livres d''images. Papiers, crayons, peinture/'
),
(
    5,
    6,
    'Introduire les règles de la politesse et de respect des autres. Les interdits et les dangers sont clairement posés et expliqués. Le respect des jouets et des livres est encouragé. Favoriser l''autonomie de l''enfant en l''accompagnement par la parole. Favoriser le langage. L''écouter, lui parler, nommer les objets. Jouer avec lui sans faire à sa place. L''encourager et le féliciter. Lui aménager un espace bien à lui. Proposer des jeux sans le sur-stimuler. Lui laisser le temps de s''ennuyer pour imaginer ses prochaines jeux.'
),
(
    6,
    1,
    'L''enfant court, grimpe, monte un escalier seul et participe à son déshabillage puis à l''habillage. Il commence à manger seul. A partir de 18 mois, l''enfant peut être sensibilisé à l''apprentissage de la propreté.'
),
(
    6,
    3,
    'Son vocabulaire s''enrichit peu à peu.'
),
(
    6,
    4,
    'L''enfant comprend un ordre simple puis de plus en plus complexe. Il commence à associer deux mots pours faire une phrase. Il est dans la période d''autonomie (il veut faire tout seul). Il reste dans la période d''opposition. Il tolère mieux la séparation d''avec sa mère. Il apprécie la compagnie d''autres enfants. Il se cherche derrière le miroir.'
),
(
    6,
    5,
    'Dînette. Porteurs. Ballon. Jouets à tirer et à pousser. Jeux d''encastrements, d''assemblages et de manipulations.'
),
(
    6,
    6,
    'Respecter le rythme propre de chaque enfant. Favoriser l''autonomie en l''accompagnant par la parole. Prévenir les accidents domestiques (vigilance, expliquer le risque à l''enfant). Favoriser la socialisation.'
),
(
    7,
    1,
    'L''enfant saute à pieds joints. Il fait du tricycle. Il monte et descend les escaliers. Il enlève ses chaussures. Il acquiert peu à peu la propreté de jour puis de nuit. Il dessine, imite un trait.'
),
(
    7,
    3,
    'L''enfant enrichit son vocabulaire et commence à faire des phrases. Il utilise le "je".'
),
(
    7,
    4,
    'Il est attentif à l''autre. Il sait s''il est une fille ou un garçon et explore son corps. Il trouve du plaisir à dire "non".'
),
(
    7,
    5,
    'Activités manuelles : collage, gommettes, dessins, pâte à modeler. Il reprend des activités d''intérieur avec un besoin de création. Il aime montrer ce qu''il peut créer et commence à s''introduire dans le monde des adultes en écrivant ou en transportant des choses lourdes. Il est important à ce moment d''initier l''enfant aux jeux de société qui lui apprendront à comprendre des règles de jeu et à les respecter. Jeux d''imitation : poupées, dînettes, garages et déguisements. Ballon. Tricycle. Tablette.'
),
(
    7,
    6,
    'Accompagner l''enfant dans tous ses apprentissages et découvertes. L''accompagner dans la socialisation : aire de jeux, parcs, bibliothèques, etc.'
),
(
     8,
     1,
     'L''enfant pédale sur une bicyclette sans roues latérales. Il saute à cloche-pied. Il peut lacer ses chaussures. Il tape une balle dans un but. Il boutonne ses vêtements. Ses dessins s''affinent.'
),
(
    8,
    3,
    'L''enfant a un langage correct, riche, qui lui permet de mieux communiquer avec les autres. Il utilise correctement le temps des verbes.'
),
(
    8,
    4,
    'Il est d''avantage capable de comprendre les intentions des autres. Il commence à s''identifier au parent du même sexe et s''intéresse à sa façon de faire. Il comprend, contrôle et surmonte mieux ses émotions. Il distingue les notions haut/bas, matin/après-midi. Il reconnait la droite et la gauche. Les couleurs sont connues. Il entre dans l''âge de la curiosité : l''âge du pourquoi et des questions multiples. Il développe son imaginaire.'
),
(
    8,
    5,
    'Livres. Jeux d''extérieur : escalade, vélo, corde. Jeux de société : loto, domino, memory, petits chevaux. Déguisements, jeux d''imitation (marchand, cuisine). Collage et petits personnages articulés. Tablette, télévision, ordinateur sont dans le salon et utilisés sous contrôle parental. Des règles claires sont fixées pour les temps d''utilisation.'
),
(
    8,
    6,
    'Nourrir l''imaginaire de l''enfant par des histoires, lectures auxquelles l''enfant va s''identifier. Poursuivre l''accompagnement des apprentissages et découvertes en lien avec l''école maternelle. L''accompagnement de l''adulte est nécessaire et l''enfant apprécie de partager ses temps de jeux.'
);




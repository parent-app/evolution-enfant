CREATE SCHEMA IF NOT EXISTS child_vaccine;

CREATE TABLE child_vaccine.t_vaccine
(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    mandatory BOOLEAN NOT NULL
);

CREATE TABLE child_vaccine.t_vaccination_period
(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR(45) NOT NULL
);

CREATE TABLE child_vaccine.t_vaccine_period
(
    id BIGSERIAL PRIMARY KEY,
    vaccine_id BIGSERIAL NOT NULL,
    period_id BIGSERIAL NOT NULL,
    booster_dose BOOLEAN NOT NULL,
    CONSTRAINT FK_VACCINE_PERIOD_VACCINE FOREIGN KEY (vaccine_id) REFERENCES child_vaccine.t_vaccine(id),
    CONSTRAINT FK_VACCINE_PERIOD_PERIOD FOREIGN KEY (period_id) REFERENCES child_vaccine.t_vaccination_period(id)
);

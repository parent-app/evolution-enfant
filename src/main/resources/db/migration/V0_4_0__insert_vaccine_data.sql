INSERT INTO child_vaccine.t_vaccine(name, description, mandatory)
VALUES
('Diphtérie-Tétanos-Poliomyélite', 'Jusque dans les années 1930, la diphtérie était responsable d’une mortalité élevée chez les enfants (plusieurs milliers de cas par an). Elle a disparu en France grâce à la vaccination systématique depuis 1945, mais il existe encore des zones du monde où elle sévit, notamment en Asie du Sud-Est et en Afrique. La diphtérie est une maladie due à une bactérie (Corynebacterium diphtheriae). Très contagieuse, elle se transmet de personne à personne par la toux et les éternuements. Le tétanos est une maladie encore présente partout dans le monde. En France, entre 2008 et 2011, 36 cas de tétanos ont été déclarés, tous chez des personnes dont la vaccination n’était pas à jour, et parmi lesquelles 11 sont décédées. Le tétanos est une maladie aiguë grave, souvent mortelle en l’absence de prise en charge lourde (réanimation). La poliomyélite, qui a aujourd’hui disparu en France, devrait être la deuxième maladie éradiquée grâce à la vaccination, mais quelques rares foyers épidémiques persistent dans le monde. La poliomyélite est une maladie due à l’un des trois poliovirus existants, principalement présents dans les selles des personnes infectées. La transmission se fait par l’ingestion d’aliments, d’eau ou de boissons contaminés par les selles d’une personne porteuse du virus. La transmission peut également se faire à partir des virus présents dans la gorge des individus infectés.', true),
('Coqueluche', 'De 1996 à 2012, environ 10.000 cas de coqueluche sont survenus chez des bébés de moins de 6 mois en France, dont 18% ont été admis en service de réanimation. La coqueluche reste une des premières causes de décès par infection bactérienne chez le nourrisson de moins de 3 mois. La coqueluche est une infection respiratoire due à une bactérie (Bordetella pertussis) très contagieuse. Elle se transmet de personne à personne, en particulier par les gouttelettes de salive émises lors de la toux.', true),
('Haemophilus influenzae de tybe b (HIB)', 'Avant la mise à disposition du vaccin, la bactérie Haemophilus influenzae b était responsable de 500 à 600 méningites par an chez le jeune enfant. La bactérie Haemophilus influenzae de type b, appelée couramment « Hib », est très répandue. Elle se retrouve facilement dans les voies aériennes supérieures (nez, gorge). Elle peut être transmise par les gouttelettes de salive des personnes infectées.', true),
('Hépatite B', 'En France, on estime que près de 280 000 personnes sont porteuses d’une hépatite B chronique et que, chaque année, près de 1 500 décès sont liés à l’hépatite B. L’hépatite B est une infection du foie causée par le virus de l’hépatite B (VHB). Ce virus se transmet par le sang et par les autres fluides corporels, essentiellement les sécrétions vaginales et le sperme.', true),
('Pneumocoque', 'Les méningites sont la forme la plus grave des infections à pneumocoques : 1 enfant atteint sur 10 en meurt et 1 sur 3 en garde des séquelles sévères. Le pneumocoque est une bactérie responsable d’infections fréquentes telles que des otites, des sinusites, des pneumonies et aussi des septicémies ou des méningites (infections de l’enveloppe du cerveau). Ces infections touchent plus souvent les jeunes enfants, les personnes âgées et les personnes atteintes de maladies chroniques ou qui suivent un traitement qui diminue leurs défenses contre les infections.', true),
('Méningocoque C', 'En France, les infections graves à méningocoques touchent environ 600 personnes par an (deux tiers de méningites, un tiers de septicémies). Les personnes les plus touchées sont les nourrissons de moins d’1 an, les enfants d’1 à 4 ans et les adolescents et jeunes adultes de 15 à 24 ans. Les infections à méningocoques sont dues à une bactérie, Neisseria meningitidis, principalement de sérogroupes A, B, C, W et Y. En France, les principaux sérogroupes sont le B et le C.', true),
('Rougeole-Oreillons-Rubéole', 'Suite aux années épidémiques de 2018 et 2019, on observe un effondrement du nombre de cas de rougeole depuis avril 2020, en lien avec les mesures barrières mises en place pour lutter contre l''épidémie de Covid-19. La rougeole est une maladie très contagieuse due à un virus qui se transmet très facilement par la toux, les éternuements et les sécrétions nasales, Une personne contaminée par la rougeole peut infecter entre 15 et 20 personnes ! Avant la vaccination, les oreillons étaient la première cause de méningite virale (infection des enveloppes du cerveau) chez l’enfant. La maladie a aujourd’hui pratiquement disparu en France grâce à la vaccination. Des cas surviennent exceptionnellement chez des adultes jeunes non vaccinés ou qui ont perdu la protection acquise grâce à la vaccination dans l’enfance. Les oreillons correspondent à une maladie très contagieuse due à un virus (virus « ourlien »). Elle se transmet par l’intermédiaire de gouttelettes de salive contenant du virus. Dans 20 à 30% des cas, il n’y aucun symptôme et l’infection passe inaperçue. La vaccination a fait quasiment disparaître la rubéole congénitale en France. Cependant, la rubéole occasionne encore chaque année des infections chez les fœtus et des interruptions médicales de grossesse. La rubéole est une maladie contagieuse très fréquente due à un virus. Avant la vaccination, elle touchait le plus souvent les enfants. Présente partout dans le monde, elle se transmet surtout par voie aérienne respiratoire lors de contacts avec une personne porteuse du virus, qu’elle ait ou non des symptômes. En effet, dans environ la moitié des cas, les personnes infectées ne présentent pas de signe.', true);

INSERT INTO child_vaccine.t_vaccination_period(label)
VALUES
('2 mois'),
('4 mois'),
('5 mois'),
('11 mois'),
('12 mois'),
('16-18 mois'),
('6 ans'),
('11-13 ans');

INSERT INTO child_vaccine.t_vaccine_period(vaccine_id, period_id, booster_dose)
VALUES
(1, 1, false),
(1, 2, false),
(1, 4, false),
(1, 7, true),
(1, 8, true),

(2, 1, false),
(2, 2, false),
(2, 4, false),
(2, 7, true),
(2, 8, true),

(3, 1, false),
(3, 2, false),
(3, 4, false),

(4, 1, false),
(4, 2, false),
(4, 4, false),

(5, 1, false),
(5, 2, false),
(5, 4, false),

(6, 3, false),
(6, 5, false),

(7, 5, false),
(7, 6, false);

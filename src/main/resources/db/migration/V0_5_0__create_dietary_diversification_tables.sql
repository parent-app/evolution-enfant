CREATE SCHEMA IF NOT EXISTS dietary_diversification;

CREATE TABLE dietary_diversification.t_diversification_period
(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR(50) NOT NULL
);

CREATE TABLE dietary_diversification.t_food_type
(
    id BIGSERIAL PRIMARY KEY,
    label VARCHAR(50) NOT NULL
);

CREATE TABLE dietary_diversification.t_food
(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    type_id BIGSERIAL NOT NULL,
    comment VARCHAR(255),
    period_id BIGSERIAL NOT NULL,
    CONSTRAINT FK_FOOD_TYPE FOREIGN KEY (type_id) REFERENCES dietary_diversification.t_food_type(id),
    CONSTRAINT FK_FOOD_PERIOD FOREIGN KEY (period_id) REFERENCES dietary_diversification.t_diversification_period(id)
);

ALTER TABLE child_vaccine.t_vaccination_period
ADD month_value SMALLINT;

UPDATE child_vaccine.t_vaccination_period as vp
SET month_value = c.month_value
FROM (values
     (1, 2),
     (2, 4),
     (3, 5),
     (4, 11),
     (5, 12),
     (6, 16),
     (7, 72),
     (8, 132)
 ) as c(id, month_value)
WHERE c.id = vp.id;

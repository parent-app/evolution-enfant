ALTER TABLE child_growth.t_growth_stage_period
ADD min_month_value SMALLINT,
ADD max_month_value SMALLINT;

UPDATE child_growth.t_growth_stage_period as gsp
SET min_month_value = c.min_month_value,
    max_month_value = c.max_month_value
FROM (values
     (1, 0, 3),
     (2, 3, 6),
     (3, 6, 9),
     (4, 9, 12),
     (5, 12, 18),
     (6, 18, 24),
     (7, 24, 36),
     (8, 36, 72)
 ) as c(id, min_month_value, max_month_value)
WHERE c.id = gsp.id;

ALTER TABLE dietary_diversification.t_diversification_period
    ADD min_month_value SMALLINT,
    ADD max_month_value SMALLINT;

UPDATE dietary_diversification.t_diversification_period as dp
SET min_month_value = c.min_month_value,
    max_month_value = c.max_month_value
FROM (values
      (1, 0, 4),
      (2, 4, 6),
      (3, 4, 7),
      (4, 6, 12),
      (5, 7, 12),
      (6, 12, 18),
      (7, 18, 24),
      (8, 24, 72)
     ) as c(id, min_month_value, max_month_value)
WHERE c.id = dp.id;

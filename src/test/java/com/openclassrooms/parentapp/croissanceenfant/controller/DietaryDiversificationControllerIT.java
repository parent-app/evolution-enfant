package com.openclassrooms.parentapp.croissanceenfant.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetFoodDto;
import com.openclassrooms.parentapp.croissanceenfant.exception.AppEntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DietaryDiversificationControllerIT {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void getFoods_withPeriod1() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE)
                .param("period", "1"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].id", is(163)))
                .andExpect(jsonPath("$[1].id", is(165)))
                .andReturn();
    }

    @Test
    void getFoods_withType10() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE)
                .param("type", "10"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(6)))
                .andReturn();
    }

    @Test
    void getFoods_withPeriod8AndType10() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE)
                .param("period", "8")
                .param("type", "10"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn();
    }

    @Test
    void getPeriods() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE + "/" + DietaryDiversificationController.PERIOD_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(8)))
                .andReturn();
    }

    @Test
    void getTypes() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE + "/" + DietaryDiversificationController.TYPE_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(10)))
                .andReturn();
    }

    @Test
    void suggestFood() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE + "/suggest")
                .param("name", "bric"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("Abricot")))
                .andExpect(jsonPath("$[1].name", is("Abricot sec")))
                .andReturn();
    }

    @Test
    void getFoodById() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE + "/26"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", is(26)))
                .andExpect(jsonPath("$.name", is("Navet")))
                .andReturn();
    }

    @Test
    void getFoodById_throwsEntityNotFoundException() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE + "/25546"))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(AppEntityNotFoundException.class))
                .andExpect(result -> assertThat(result.getResolvedException().getMessage()).containsSequence("Entity not found"))
                .andReturn();
    }

    @Test
    void getChildGrowthStage() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(DietaryDiversificationController.ROOT_RESOURCE + DietaryDiversificationController.CHILD_RESOURCE)
            .param("birthDate", LocalDate.now().minusMonths(5).toString()))
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
            .andExpect(jsonPath("$", hasSize(38)))
            .andDo(MockMvcResultHandlers.print())
            .andReturn();

        WidgetFoodDto[] widgetFoodDtos = objectMapper.readValue(result.getResponse().getContentAsByteArray(), WidgetFoodDto[].class);
        assertThat(Arrays.stream(widgetFoodDtos).map(WidgetFoodDto::getPeriod).distinct().collect(Collectors.toList())).contains("4 - 6 mois", "4 - 7 mois");
    }

}

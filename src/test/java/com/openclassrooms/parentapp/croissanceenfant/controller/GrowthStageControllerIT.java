package com.openclassrooms.parentapp.croissanceenfant.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetFoodDto;
import com.openclassrooms.parentapp.croissanceenfant.dto.WidgetGrowthStageDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GrowthStageControllerIT {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    void getGrowthStages() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(40)))
                .andReturn();
    }

    @Test
    void getGrowthStages_filterByPeriodId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE)
                .param("periods", "5"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(5)))
                .andReturn();
    }

    @Test
    void getGrowthStages_filterByPeriodId_multiplePeriods() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE)
                .param("periods", "1, 3"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(10)))
                .andReturn();
    }

    @Test
    void getGrowthStages_filterByTypeId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE)
                .param("types", "1"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(8)))
                .andReturn();
    }

    @Test
    void getGrowthStages_filterByTypeId_multipleTypes() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE)
                .param("types", "3, 4"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(13)))
                .andReturn();
    }

    @Test
    void getGrowthStagePeriods() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE + GrowthStageController.PERIOD_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(8)))
                .andReturn();
    }

    @Test
    void getGrowthStageTypes() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE + GrowthStageController.TYPE_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(6)))
                .andReturn();
    }

    @Test
    void getChildGrowthStage() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(GrowthStageController.ROOT_RESOURCE + GrowthStageController.CHILD_RESOURCE)
                .param("birthDate", LocalDate.now().minusMonths(5).toString()))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(5)))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        WidgetGrowthStageDto[] widgetGrowthStageDtos = objectMapper.readValue(result.getResponse().getContentAsByteArray(), WidgetGrowthStageDto[].class);
        assertThat(Arrays.stream(widgetGrowthStageDtos).map(WidgetGrowthStageDto::getPeriod).distinct().collect(Collectors.toList())).contains("3 à 6 mois");
    }
}

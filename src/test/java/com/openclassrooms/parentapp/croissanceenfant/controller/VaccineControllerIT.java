package com.openclassrooms.parentapp.croissanceenfant.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class VaccineControllerIT {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void getVaccines() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(VaccineController.ROOT_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(7)))
                .andReturn();
    }

    @Test
    void getVaccines_filterByPeriod() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(VaccineController.ROOT_RESOURCE)
                .param("periods", "1, 2"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(5)))
                .andReturn();
    }

    @Test
    void getPeriods() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(VaccineController.ROOT_RESOURCE + VaccineController.PERIOD_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(8)))
                .andReturn();
    }

    @Test
    void getChildNextVaccines() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(VaccineController.ROOT_RESOURCE + VaccineController.CHILD_RESOURCE)
                .param("birthDate", LocalDate.now().minusYears(2).toString()))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)));
    }

}
